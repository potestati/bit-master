<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link href="lib/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="lib/bootstrap-3.0.3/css/bootstrap-theme.min.css" rel="stylesheet" />
        <link href="blog.css" rel="stylesheet" />
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>
        <?php
        define("DB_SERVER", "localhost");
        define("DB_USER", "root");
        define("DB_PASS", "");
        define("DB_IME", "bit_master");

        $conn = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_IME);
        if (mysqli_connect_errno()) {
            die("Konekcija nije uspela: " .
                    mysqli_connect_error() .
                    " (" . mysqli_connect_errno() . ")"
            );
        }

// define variables and set to empty values
        $nameErr = $emailErr = $subjectErr = $messageErr = "";
        $name = $email = $subject = $message = "";
        $_SESSION["poruka"] = '';

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["name"])) {
                $nameErr = "Name is required";
            } else {
                $name = test_input($_POST["name"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                    $nameErr = "Only letters and white space allowed";
                }
            }
            if (empty($_POST["email"])) {
                $emailErr = "Email is required";
            } else {
                $email = test_input($_POST["email"]);
                // check if e-mail address is well-formed
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid email format";
                }
            }
            if (empty($_POST["subject"])) {
                $subjectErr = "Name is required";
            } else {
                $subject = test_input($_POST["subject"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $subject)) {
                    $subjectErr = "Only letters and white space allowed";
                }
            }
            if (empty($_POST["message"])) {
                $message = "";
            } else {
                $message = test_input($_POST["message"]);
                if (!preg_match("/^[a-zA-Z ]*$/", $message)) {
                    $messageErr = "Only letters and white space allowed";
                }
            }

//            mysql_select_db("register", $conn);
            $sql = "INSERT INTO register (ime, email, subject, message) VALUES ('$name', '$email', '$subject', '$message')";
//    var_dump($sql);

            if (mysqli_query($conn, $sql)) {
//                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }

            $run_query = mysqli_close($conn);

            if ($run_query) {
                $_SESSION["poruka"] = "<span style='color: red;'>Hvala sto ste se registrovali ubrzo cete dobiti odgovor na email adresu!!!</span>";

                function clean_string($string) {
                    $bad = array("content-type", "bcc:", "to:", "cc:", "href");
                    return str_replace($bad, "", $string);
                }

                //send email
                /*
                  function clean_string($string) {
                  $bad = array("content-type", "bcc:", "to:", "cc:", "href");
                  return str_replace($bad, "", $string);
                  }

                  $email_message .= "Ime i prezime: " . clean_string($name) . "\n";
                  $email_message .= "Email: " . clean_string($email) . "\n";
                  $email_message .= "Subjekt: " . clean_string($subject) . "\n";
                  $email_message .= "Poruka: " . clean_string($message) . "\n";

                  // create email headers
                  $headers = 'From: ' . $email . "\r\n" .
                  'Reply-To: ' . $email . "\r\n" .
                  'X-Mailer: PHP/' . phpversion();
                  @mail($email, $subject, $message, $headers);



                 */

//                header("Location: " . "index.php");
                
                $name = $email = $subject = $message = '';
                
            }
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);

            return $data;
        }
        ?>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="index.php">
                            <img src="img/logo.png" alt="logo">
                        </a>
                    </div>
                    <?php include 'menu-main.php'; ?>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a> <span class="divider">/</span></li>
                        <li><a href="#">Kategorije</a> <span class="divider">/</span></li>
                        <li class="active">Online Prijava</li>
                    </ul>
                </div>
            </div>
            <div class="inner-img">
                <img src="img/contact.jpg">
            </div>
        </header>
        <div style="text-align: center;"><?php echo $_SESSION["poruka"]; ?></div>

        <div class="contact-page"  style="background: #fff;">
            <div class="container">
                <h1>Contact Me</h1>
                <div class="well">
                    <p class="lead">
                        Zelis da se prijavis na kurs ili da nam postavis pitanje? Upotrebi kontakt formu ispod i posalji nam email. Mi cemo ti odgovoriti brzo.
                    </p>
                </div>
                <div class="contact-form">
                    <p><span class="error">* Popuni molim te polja oznacena sa zvezdicom.</span></p>

                    <form class="form-horizontal" role="form" method="post" 
                          action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" 
                                       placeholder="First & Last Name" value="<?php echo $name; ?>">
                                <span class="error">* <?php echo $nameErr; ?></span>
                                <?php
                                if (isset($_POST["submit"])) {
                                    if (strlen($name) < 2) {
                                        echo "Please enter 2 caracters as minimum.";
                                    } elseif (strlen($name) > 20) {
                                        echo "Please enter less than 20 caracters.";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email" 
                                       placeholder="example@domain.com" value="<?php echo $email; ?>">
                                <span class="error">* <?php echo $emailErr; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-sm-2 control-label">Subject</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="website" 
                                       name="subject" placeholder="subject" 
                                       value="<?php echo $subject; ?>">
                                <span class="error"><?php echo $subjectErr; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-2 control-label">Message</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="4" name="message">
                                    <?php echo $message; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group1">
                            <div class="col-sm-10 col-sm-offset-2">
                                <input id="submit" name="submit" type="submit" value="Submit" class="btn rezervisi">
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <?php include('footer.php'); ?>
        <!-- Jquery and Bootstrap Script files -->
        <script src="lib/jquery-2.0.3.min.js"></script>
        <script src="lib/bootstrap-3.0.3/js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>