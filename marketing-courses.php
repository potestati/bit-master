<!DOCTYPE html>
<html class="demo-1 no-js">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="index.php">
                            <img src="img/logo.png" alt="logo">
                        </a>
                    </div>
                    <?php include 'menu-main.php'; ?>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a> <span class="divider">/</span></li>
                        <li><a href="#">Kategorija</a> <span class="divider">/</span></li>
                        <li class="active">Marketing</li>
                    </ul>
                </div>
            </div>
        </header>
        <section class="post-content-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <img class="inner-header"src="img/marketing-kursevi.jpg" class="img-fluid" alt="Marketing Kursevi">
                        <p>
                            <strong>&#34;Ne razumem zasto se ljudi boje novih ideja. Ja se bojim starih.&#34;</strong>
                            U danasnjem zivotu i biznisu promene se odvijaju prebrzo, paznja korisnika je sve slabija, pobedjuju NAJKREATIVNIJI!                        
                        </p>
                        <div class="well ">
                            <large>
                                <h2 class="heading-course">
                                    INTERNET MARKETING
                                </h2>
                            </large>
                        </div>
                        <p>
                            Tradicionalni marketing &#34;cilja stotinu da bi pogodio jednog“, dok savremeni marketing „cilja jednog da bi pogodio stotine!&#34;                       
                        </p>
                        <blockquote>
                            <p>
                                Sadrzaj ispred forme, precizno odredjivanje ciljne grupe, efikasnija prodaja uz smanjenje troskova, kako najbolje iskoristiti drustvene mreze, izgradnja baze lojalnih kupaca.
                            </p>
                            <footer>
                                Sve ovo
                                <p>
                                    i jos mnogo drugih odgovora spremili smo za vas na kursu <cite title="Source Title">INTERNET MARKETING.</cite>
                                </p>
                            </footer>
                            <div class="col-sm-10 col-sm-offset-2">
                                <a href="u-pripremi.php" class="btn rezervisi">Detaljnije o kursu</a>
                            </div>
                        </blockquote>
                        <!-- two courses -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="progr/OSNOVE COPYWRITING.pdf" target="_blank">
                                            <img src="img/copyrighting.jpg" alt="Marketing Copywriting">
                                            <div class="caption">
                                                <h2 class="heading-course">COPYWRITING</h2>
                                                <h3>Pisani trag moze da ostavi veoma snazan utisak na čitaoca i zbog toga pisanje ima veliku moc.</h3>
                                                <p>
                                                    Pisanje je zanat koji se moze dovesti do savrsenstva, koriscenjem osnovnih tehnika i neprestanim usavrsavanjem.
                                                    Naucite vestinu pisanja prodajnih tekstova i otvorite sebi vrata za jedno od najtrazenijih zanimanja danas.
                                                    Na kursu <strong>OSNOVE COPYWRITING - a</strong> savladacete pravila za pisanje tekstova u marketinske svrhe. 
                                                    Naucicete kako da doprete do svoje publike, kojim tonom da im se obracate i kako da ih pozovete na akciju.
                                                </p>
                                            </div>
                                            Vidi vise o kursu ...
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="u-pripremi.php" target="_blank">
                                            <img src="img/seo.jpg" alt="Marketing SEO">
                                            <div class="caption">
                                                <h2 class="heading-course">SEO</h2>
                                                <br>
                                                <h3>Nekada nije vazno samo ucestvovati... Vazno je biti PRVI! </h3>
                                                <p>
                                                    U danasnje vreme, gotovo je nezamislivo obavljati neku delatnost, a pritom ne koristiti pogodnosti internet oglasavanja. Iako internet marketing pruza brojne mogucnosti, ipak nije dovoljno samo biti tu. Potrebno je biti PRVI. Na PRVOM MESTU, NA PRVOJ STRANICI Google – a.
                                                    <br><br><br>
                                                    Zato postoji SEO (Search Engine Optimization) i zato vam „BIT MASTER“ nudi KURS SEO optimizacije.
                                                </p>
                                            </div>
                                            Vidi vise o kursu ...
                                        </a>
                                    </div>
                                </div>
                                <div style="display: none;"class="col-md-8">
                                    <div class="thumbnail">
                                        <a href="#" target="_blank">
                                            <img src="img/php-osnove.jpg" alt="web-developer">
                                            <div class="caption">
                                                <h2 class="heading-course">SEO</h2>
                                                <h3>Nekada nije vazno samo ucestvovati... Vazno je biti PRVI! </h3>
                                                <p>
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </div>
                                            Vidi vise o kursu ...
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4"  style="display: none;">
                                    <div class="thumbnail">
                                        <a href="#" target="_blank">
                                            <img src="img/front-end-developer.jpg" alt="web-developer">
                                            <div class="caption">
                                                <h2 class="heading-course">SEO</h2>
                                                <br>
                                                <h3>Nekada nije vazno samo ucestvovati... Vazno je biti PRVI! </h3>
                                                <p>
                                                    U danasnje vreme, gotovo je nezamislivo obavljati neku delatnost, a pritom ne koristiti pogodnosti internet oglasavanja. Iako internet marketing pruza brojne mogucnosti, ipak nije dovoljno samo biti tu. Potrebno je biti PRVI. Na PRVOM MESTU, NA PRVOJ STRANICI Google – a.
                                                    <br><br><br>
                                                    Zato postoji SEO (Search Engine Optimization) i zato vam „BIT MASTER“ nudi KURS SEO optimizacije.
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div  style="display: none;" class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="#" target="_blank">
                                            <img src="img/wordpress.jpg" alt="wordpress">
                                            <div class="caption">
                                                <h2>Wordpress</h2>
                                                <h3>Napravite web-site za jedan dan</h3>
                                                <p>
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div style="display: none;" class="col-md-8">
                                    <div class="thumbnail">
                                        <a href="#" target="_blank">
                                            <img src="img/web-developer.jpg" alt="web-developer">
                                            <div class="caption">
                                                <h2>Osnove PHP & MySQL</h2>
                                                <p>
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- two courses-->
                        <blockquote>
                            <h2 class="heading-course">NAJKREATIVNIJI SAMO OSTAJU!</h2>
                            <p>
                                Ako bismo jednom recenicom morali da opisemo situaciju u svetu marketinga danas, citirali bismo Brajana Solisa, poznatog digitalnog analiticara: "Engage (or disappear) - Prilagodi se (ili nestani)". Promene su prebrze, paznja kupaca sve slabija, pobedjuju najkreativniji!                             
                            </p>

                        </blockquote>

                        <div class="image-block">
                            <img style="width: 850px; height: 560px;"src="img/marketing-copyright.jpg" class="img-responsive" >
                        </div>
                        <p>
                            Zato je kreativna industrija medju najpopularnijim i prema zvanicnim podacima jedna od najbrze rastucih izvoznih u Srbiji. Na kursevima koje vam je pripremio BiT master postanite deo ovog sveta, naucite kako da povecate vidljivost svom biznisu i brendu, kako da dovedete svoje reklamne tekstove i poruke do savrsenstva, kako da targetirate zeljene potencijalne kupce, kako da iskoristite drustvene mreze za efikasnu promociju. I sto je najvaznije: kako da smanjite troskove a povecate prodaju!
                            Za vas cemo pratiti trendove, usavrsavati postojece kurseve, kreirati nove i redovno vas informisati o svim vaznim desavanjima iz oblasti INTERNET MARKETINGA.
                            Zato iskoristite PROMOTIVNE uslove do KRAJA GODINE i vec DANAS upisite jedan od kurseva koji startuju u JANUARU.                        
                        </p>
                    </div>
                    <?php include 'sidebar.php'; ?>
                </div>
            </div> <!-- /container -->
        </section>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <?php include('footer.php'); ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="js/snap.svg-min.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
        <!-- / Script-->
    </body>
</html>
