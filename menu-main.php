<div class="col-md-8 right">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Home</a></li>
                <li><a href="o-nama.php">O Nama</a></li>
                <li class="dropdown">
                    <a href="courses.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kursevi <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="finance-courses.php">Business</a></li>
                        <li><a href="marketing-courses.php">Marketing</a></li>
                        <li><a href="it-courses.php">IT&#47;DIZAJN</a></li>
                    </ul>
                </li>
                <!--                <li><a href="#">Blog</a></li>-->
                <li><a href="cenovnik.php">Cenovnik</a></li>
                <li><a href="register.php">Kontakt</a></li>
            </ul>
        </div>
    </div>
</div>
