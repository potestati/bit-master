<!DOCTYPE html>
<html class="demo-1 no-js">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="index.php">
                            <img src="img/logo.png" alt="logo">
                        </a>
                    </div>
                    <?php include 'menu-main.php'; ?>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a> <span class="divider">/</span></li>
                        <li><a href="#">Kategorija</a> <span class="divider">/</span></li>
                        <li class="active">Business</li>
                    </ul>
                </div>
            </div>
        </header>
        <section class="post-content-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <img class="inner-header"src="img/kursevi-finansija.jpg" class="img-fluid" alt="Kursevi Finansija">
                        <h1>KURSEVI FINANSIJA RACUNOVODSTVA I BIZNISA</h1>
                        <h2 class="heading-course">
                            OSNOVE FINANSIJSKE ANALIZE:
                        </h2>
                        <div class="well ">
                            <large>RACIO LIKVIDNOSTI 1,36, KAPITALIZACIJA 37%, BRUTO MARZA 21%, EBITDA MARZA 12%, NAPLATA OD KUPACA 55 DANA... </large>
                        </div>
                        <p>
                            Ne dozvolite da vas zbune ovi brojevi. Vrlo je jednostavno – za mesec dana savladacete pokazatelje poslovanja preduzeca i njihovo osnovno znacenje.                         
                        </p>
                        <blockquote>
                            <p>Znanja i vestine koje sticete na ovoj obuci su:</p>
                            <footer>
                                <cite title="Source Title">Uz pomoc strucnosti predavaca,
                                    <ul>
                                        <li>
                                            Upoznacete se sa pojmovima koji cine finansijsku analizu - vrstama bilansa, bilansom stanja i uspeha, kapitalom, obrtnim kapitalom i izveštajima o novčanim tokovima.
                                        </li>
                                        <li>
                                            Shvaticete koje vrste pravnih formi preduzeca postoje, ko su korisnici finansijskih izveštaja i zasto su oni znacajni.
                                        </li>
                                        <li>
                                            Naucicete osnovne racio pokazatelje - horizontalnu i vertikalnu analizu.
                                        </li>
                                        <li>
                                            Predavanja prate primeri iz realnog poslovanja preduzeca. Polaznici aktivno ucestvuju u nastavi i diskusijama.
                                        </li>
                                    </ul>
                                </cite>
                            </footer>
                            <p>
                                Nakon odslusanog kursa, polaznik ce biti obucen da samostalno tumaci bilanse preduzeca i na taj nacin vrlo jednostavno odredi osnovne pokazatelje poslovanja: da li je preduzece profitabilno, likvidno ili zaduzeno. Bicete u mogucnosti da sagledate poslovni ciklus preduzeca u datom momentu i da utvrdite trend poslovanja.
                            </p>
                            <div class="col-sm-10 col-sm-offset-2">
                                <a href="progr/OSNOVE FINANSIJSKE ANALIZE.pdf" class="btn rezervisi">Detaljnije o kursu</a>
                            </div>
                        </blockquote>
                        <!-- two courses -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="progr/FINANSIJSKA ANALIZA - napredni nivo.pdf" target="_blank">
                                            <img src="img/napredni-finan.jpg" alt="Napredni Finansiska Analiza">
                                            <div class="caption">
                                                <h2 class="heading-course">NAPREDNA FINANSIJSKA ANALIZA:</h2>
                                                <h3>Uz konstantan rad na licnom razvoju, svetske berze u Frankfurtu, Njujorku ili Tokiju, neće vam biti nedostizne.</h3>
                                                <p>
                                                <ul>
                                                    Ako ste zavrsili kurs „Osnove finansijske analize“ ili vec imate predznanje iz ove oblasti, spremni ste za korak dalje –  kurs „Napredna finansijska analiza“.</li>
                                                    <li>Upoznajte se sa Cash flow – om (izvetajem o novcanim tokovima)</li>
                                                    <li>Naucite da projektujete bilans stanja i bilans uspeha</li>
                                                    <li>Saznajte kako se radi reklasifikacija bilansnih pozicija i kada se ona primenjuje</li>
                                                </ul>
                                                Vidi vise informacija o kursu...
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="progr/BIZNIS PLAN.pdf" target="_blank">
                                            <img src="img/business-plan.jpg" alt="Business Plan">
                                            <div class="caption">
                                                <h2 class="heading-course">BIZNIS PLAN – IZRADA I IMPLEMENTACIJA</h2>
                                                <h3>&#34;Ne planirati znaci planirati neuspeh&#33;&#34;</h3>
                                                <p>
                                                    Za ostvarenje ciljeva i snova u zivotu nisu dovoljni samo zelja i ideja. Potreban je PLAN.
                                                    Pocetnik ste i tek pokrecete poslovanje&#63; Vas biznis je na raskrsnici i imate dilemu koji novi proizvod da uvedete, koje trziste da odaberete&#63; Pitate se koji je period povrata investicije u novi objekat ili opremu&#63;
                                                    Treba da odobrite klijentu kredit u banci, ali ne znate koliki su ocekivani novcani tokovi iz buduceg poslovanja&#63;
                                                    <br>
                                                    <strong>Kurs &#34;BIZNIS PLAN – IZRADA I IMPLEMENTACIJA&#34; dace vam odgovore na sva pitanja.</strong>
                                                    <br><br>
                                                    Vidi vise informacija o kursu...
                                                </p>

                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="thumbnail">
                                        <a href="progr/Vestine prodaje i pregovaranja.pdf" target="_blank">
                                            <img src="img/business-pregovaranje.jpg" alt="Vestine Pregovaranja">
                                            <div class="caption">
                                                <h2 class="heading-course">VEŠTINE PRODAJE I PREGOVARANJA</h2>
                                                <h3>&#34;U zivotu i biznisu ne dobijete uvek ono sto zasluzujete, vec ono sto ispregovarate&#33;&#34;</h3>
                                                <p>
                                                <ul>
                                                    Kurs &#34;VESTINE PRODAJE I PREGOVARANJA&#34; ce vam omoguciti da savladate tehnike koje ce vam pomoci u svakodnevnom zivotu i poslu. Od predavaca sa dugogodisnjim iskustvom u prodaji i pregovaranju, kroz prakticne vezbe i radionice, naucicete&#58;</li> 
                                                    <li>Faze prodajne aktivnosti i kako da prepoznate potrebe kupaca</li>
                                                    <li>Win &#95; win prodajno razmisljanje</li>
                                                    <li>Koja je vaznost postprodaje</li>
                                                    <li>Koji stilovi pregovaranja postoje</li>
                                                    <li>Sta su pojmovi cross &#95; selling i up &#95; selling</li>
                                                    <li>Kako da prevazidjete strahove i odugovlacenje kod donosenja odluka u zivotu i poslu</li>
                                                </ul>
                                                Vidi vise informacija o kursu...
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: none;" class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="#" target="_blank">
                                            <img src="img/spoljna-trgovina.jpg" alt="Spoljna Trgovina">
                                            <div class="caption">
                                                <h2 class="heading-course">SPOLJNA TRGOVINA</h2>
                                                <h3>Kina ili Rusija&#63; Mozda EU&#63;</h3>
                                                <p>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div style="display: none;" class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="#" target="_blank">
                                            <img src="img/wordpress.jpg" alt="wordpress">
                                            <div class="caption">
                                                <h2 class="heading-course">Wordpress</h2>
                                                <h3>Napravite web-site za jedan dan</h3>
                                                <p>
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                    Lorem ipsum donec id elit non mi porta gravida at eget metus.
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="thumbnail">
                                        <a href="progr/SPOLJNOTRGOVINSKO POSLOVANJE.pdf" target="_blank">
                                            <img src="img/spoljna-trgovina.jpg" alt="Spoljna Trgovina">
                                            <div class="caption">
                                                <h2 class="heading-course">SPOLJNA TRGOVINA</h2>
                                                <h3>Kina ili Rusija&#63; Mozda EU&#63;</h3>
                                                <p>
                                                    Kurs &#34;SPOLJNA TRGOVINA&#34;&#44; koji organizuje &#34;BIT MASTER&#34;&#44; pomirice ove svetske sile. Neka vas biznis nema granica&#33;
                                                    <br><br>
                                                    Naucicete:  
                                                <ul>
                                                    <li>
                                                        Kako da analizirate poslovno okruzenje
                                                    </li>
                                                    <li>
                                                        Sta znace pojmovi: garancije, akreditivi i faktoring i INCOTERMS
                                                    </li>
                                                    <li>
                                                        Kako da organizujete spoljnotrgovinsku sluzbu
                                                    </li>
                                                    <li>
                                                        Na koji nacin da proucite ugovore o zastupnistvu (agenturi)
                                                    </li>
                                                    <li>
                                                        Kako da pripremite ucesce na sajmovima
                                                    </li>
                                                </ul>
                                                Predavanja su namenjena zaposlenima u odeljenjima za spoljnu trgovinu, vlasnicima firmi koji žele da prošire svoje poslovanje na neka od izazovnih inostranih tržišta i onima koji žele da nauče više i steknu prednost prilikom zapošljavanja.
                                                </p>
                                                <br>
                                                <br>
                                                Vidi detaljnije o kursu ...
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- two courses-->
                        <blockquote>
                            <h2 class="heading-course">KOLIKO KOSTA (NE)ZNANJE?</h2>
                            <p>
                                Sretnu se finansijski i generalni direktor. Finansijski zacudjeno upita: ’’Cemu tolika ulaganja u zaposlene, to je veoma skupo, sta ako odu iz firme?’’ Generalni direktor pogleda finansijskog, nasmeja se i odgovori: ’’Zamisli da ne ulazemo u njih a da ostanu u firmi?’’
                                Neprestani razvoj tehnike i tehnologije ucinili su da firma i ukoliko zeli da opstane na trzistu mora konstantno da se usavrsava. Kako firma tako i pojedinac. 
                            </p>
                        </blockquote>

                        <div class="image-block">
                            <img src="img/finansije.jpg" class="img-responsive" >
                        </div>
                        <p>
                            Dugogodišnje iskustvo, svakodnevni kontakt sa preduzetnicima, vlasnicima mikro, malih i srednjih preduzeca, pracenje svetskih i domacih strucnih sajtova i literature, doprineli su ovakvom konceptu Bit mastera. Zato ce vam biti dostupni kursevi i obuke koje ce voditi veoma iskusni predavaci koji su spremni da svoja znanja podele sa vama.
                            Pored kurseva i obuka koji su vec u ponudi i za koje su prijave u toku, uskoro vas ocekuju i nova iznenadjenja. Zelimo da na jednom mestu imamo kurseve koji ce se baviti HR pitanjima, finansijama i knjigovodstvom, trgovinom, marketingom, razvojem prodajnih vestina, usavrsavanjem poslovnih stranih jezika, lancem snabdevanja... 
                            Takodje, sto se tice nivoa kurseva i obuka, trudili smo se da i tu zadovoljimo razlicite potrebe. Postoji osnovni nivo namenjen pocetnicima i srednji i napredni nivoi za polaznike sa solidnim iskustvom. 
                            Praticemo trendove iz sveta BIZNISA i redovno vas informisati putem bloga.
                            Udjite u 2018. godinu spremni za nove izazove, PRIJAVITE se za jedan od BIZNIS KURSEVA u BiT masteru ODMAH po PROMOTIVNIM cenama.                        
                        </p>
                    </div>
                    <?php include('sidebar.php'); ?>
                </div>
            </div> <!-- /container -->
        </section>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <?php include('footer.php'); ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="js/snap.svg-min.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
        <!-- / Script-->
    </body>
</html>
