<!DOCTYPE html>
<html lang="en">
    <head>	
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS file -->
        <link href="lib/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="lib/bootstrap-3.0.3/css/bootstrap-theme.min.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link href="blog.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>
        <!-- Header -->
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="index.php">
                            <img src="img/logo.png" alt="logo">
                        </a>
                    </div>
                    <!--                    <div class="info-header">
                                            <a href="https://www.facebook.com/Business.IT.master/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="https://www.instagram.com/bit_master_ns/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            <a href="https://www.linkedin.com/in/bit-master-372573153/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        </div>-->
                    <?php include 'menu-main.php'; ?>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a> <span class="divider">/</span></li>
                        <li><a href="#">Kategorija</a> <span class="divider">/</span></li>
                        <li class="active">O Nama</li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- Body -->
        <div class="container">
            <h1>O Nama</h1>				
            <div class="row about-img">
                <div class="col-md-3 col-sm-3">
                    <img class="img-thumbnail img-responsive photo" src="img/rast.jpg" />
                </div>
                <div style="font-size: 16px;"class="col-md-9 col-sm-9">
                    <h2 style="margin-top: 0; font-size: 20px;">Budite spremni i sansa ce se ukazati!</h2>
                    <p>
                        <strong>B</strong>iznis i <strong>IT</strong> MASTER (BIT MASTER) je agencija, nastala kao plod dugogodisnjeg iskustva i analiziranja potreba trzista radne snage, kako osnivaca tako i predavaca. Kroz svakodnevni rad i razgovor sa preduzetnicima, mikro, malim i velikim pravnim licima, njihovim zaposlenima, studentima, djacima te pracenjem stalnih promena koje donosi savremeno poslovanje, kreirani su kursevi, obuke, treninzi i seminari koji mogu da odgovore njihovim zahtevima. 
                        Napredak tehnike i tehnologije, otvaranje novih trzista roba, usluga, kapitala i radne snage, dovode do promena u opisu radnih mesta i profila zaposlenih koji bi adekvatno odgovorili na sve potrebe poslodavaca. Te promene su sve brze i intenzivnije. Prirodno je da u takvoj situaciji covek zeli da raste, napreduje i da se usavrsava kako bi adekvatno reagovao na novonastalo stanje. Ovo nas vodi do dubljeg znacenja BIT MASTERA:  pomoci ljudima da u prijatnom ambijentu, koristeci savremenu opremu i uceci od kvalitetnih predavada, pronadju BIT (sustinu) u svojoj profesionalnoj karijeri. 
                    </p>

                    <p>
                        Prostorije BIT MASTERA se nalaze u Novom Sadu i Backoj Palanci, sa namerom da poslovanje prosirimo i u druge gradove. Pozivamo sve zainteresovane, koji imaju ideje, znanja i iskustva, a zele sve to da podele sa drugima, da nas kontaktiraju na mail adresu <a href="mailto:office@bitmaster.rs?Subject=Kursevi%20registracija" target="_top"><strong style="color: black;">office@bitmaster.rs</strong></a>   
                    </p>
                </div>
            </div>	

<!--            <p class="social-buttons text-center">
                <button type="button" class="btn rezervisi">Facebook</button> &nbsp;
                <button type="button" class="btn rezervisi">Twitter</button>
            </p>-->

            <br />
            <p class="lead text-center">
                Uvek se vodimo maksimom Vilijama Džejmsa: "Prava svrha života je posvetiti ga nečemu što će nas nadživeti." 
            </p>
            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#koncept">
                            <img src="img/cilj.jpg" class="img-responsive" />
                        </a>
                        <div class="caption text-center">
                            <h4>KONCEPT</h4>
                            <p>
                                Kursevi, obuke, treninzi i seminari u BiT MASTERU su organizovani vrlo jednostavno, centralna figura je POLAZNIK (’Klijent je najvažnij figura u našoj igri, ko ne veruje neka proba drugačije’). 
                            </p>
                            <!--                            <button class="btn btn-sm btn-success">buy now!</button>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#odgovornost">
                            <img src="img/misija.jpg" class="img-responsive" />
                        </a>
                        <div class="caption text-center">
                            <h4>DRUSTVENA ODGOVORNOST</h4>
                            <p>
                                “Make commitment to do something more important and bigger than yourself’’
                            </p>
                            <!--                            <button class="btn btn-sm btn-success">buy now!</button>-->
                        </div>								
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#master">
                            <img src="img/vizija.jpg" class="img-responsive" />
                        </a>
                        <div class="caption text-center">
                            <h4>BiT MASTER</h4>
                            <p id="koncept">
                                Postati jedan od vodećih centara za obuku i razvoj u oblastima savremenog  biznisa, menadžmenta,  marketinga i IT-ja.
                            </p>
                            <!--                            <button class="btn btn-sm btn-success">buy now!</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about container">
            <h2>KONCEPT</h2>
            <h5>Kursevi, obuke, treninzi i seminari u BiT MASTERU su organizovani vrlo jednostavno, centralna figura je POLAZNIK (’Klijent je najvažnij figura u našoj igri, ko ne veruje neka proba drugačije’).</h5> 

            <p>Svaki polaznik mora izaći sa znanjem i veštinama zbog kojih se prijavio na jedan od kurseva. To je moguće jedino uz:</p>
            <ul>
                <li>Prijatan i konforan prostor u kojem se odvijaju obuke, kursevi i treninzi.</li> 
                <li>Kvalitetnu opremu pomoću koje se bez problema i zastoja mogu obavljati i zahtevnije radnje</li>
                <li>Kvalitetnu pripremu za svaku grupu. Predavač unapred zna strukturu, nivo znanja i očekivanja polaznika te časove prilagođava njima.</li>
                <li>Kvalifikovane predavače , stručno i praktično, koji imaju prethodno bogato iskustvo u predavanjima i prenošenju znanja (’Nije znanje znanje znati, već je znanje znanje dati’).</li>
                <li>Grupe do deset polaznika</li>
                <li  id="odgovornost">Primere iz realnog života i poslovanja</li>
            </ul>
            <p>U pregovorima smo sa nekoliko firmi iz okruženja u kojima bi polaznici mogli da primene stečena znanja i veštine kroz stručnu praksu. Najbolji polaznici će imati posebne preporuke za oblasti u kojima se istaknu na zahtev trenutnih ili budućih poslodavaca.
                Takođe, postoje zainteresovani preduzetnici i pravna lica koji će omogućiti polaznicima da rade na projektima za njihove potrebe (biznis planovi, marketing kampanje, veb sajtovi i sl.).</p>
            <br>
            <br>
            <h2>DRUSTVENA ODGOVORNOST</h2>

            <p>Uvek se vodimo maksimom Vilijama Džejmsa: &#34;Prava svrha života je posvetiti ga nečemu što će nas nadživeti.&#34;</p>

            <p>U skladu sa misijom i vizijom zaposleni u BIT MASTERU imaju visoko razvijenu svest o drustvenoj odgovornosti. Zelimo da svaki pojedinac koji prodje obuku, kurs, trening ili seminar i koji shvati ozbiljnost i vaznost odluke da se menja na bolje, bude svestan da doprinosi boljem svetu i okruzenju.</p> 
            <p>Glavne odgovornosti:</p>
            <ul>
                <li>1.Pristupacne cene</li>
                <li>U vremenu u kojem zivimo i pored potrebe za brzim i neophodnim promenama, veliki broj ljudi ostaje uskracen za prakticna znanja i vestine zbog cinjenice da je zivotni standard veoma nizak. Pristupapnim cenama obuka, kurseva, treninga i seminara, zelimo da eliminisemo ovaj negativni faktor svakom pojedincu koji zeli da se usavrsava i prosiruje svoje znanje.</li> 

                <li>2.Popusti za nezaposlene, studente i srednjoskolce</li>
                <li>Pored pristupacnih cena dodatna pogodnost je namenjena i onima koji nemaju redovne prihode, nezaposlenima, studentima i srednjoskolcima.</li>

                <li>3.Humanitarne akcije</li>
                <li>Agencija ce se truditi da ucestvuje u humanitarnim akcijama koje se odnose na poboljsanje kvaliteta života najmladjih i akcije prikupljanja sredstva za lecenje dece.</li> 
                <li>Veoma je vazno da svaki polaznik zna da će deo prihoda od svih obuka, kurseva, treninga i seminara ici za neku od humanitarnih akcija.</li>

                <li id="master">4.Besplatne radionice i obuke za decu</li>

                <li>Besplatne radionice za decu. U pripremi su besplatne radionice za najmladje</li>
                <li>Obuke za najmladje. U planu su skole stranih jezika, obuke na računarima i sl.</li>
            </ul>
            <br><br>
            <h2>BiT MASTER</h2> 

            <h3>MISIJA</h3>
            &#34;Pouci coveka necem boljem ako mozes. Ako ti to nije moguce, onda razmisli da li si dobar. Cak su i bogovi prema ovakvim ljudima milostivi. Tako su dobri da im u mnogim slucajevima pomazu da dodju do zdravlja, do bogatstva, do slave. Tu mogucnost imas i ti. Reci, ko bi te u tome mogao spreciti?&#34;
            Marko Aurelije 
            BIT MASTER ce kroz <strong>obuke, kurseve, treninge i seminare</strong> pomoci sto vecem broju ljudi da steknu primenjiva znanja i vestine koje ce im omoguciti licni razvoj i napredovanje u karijeri i zivotu.
            BIT MASTER ce pruzati vrhunsku uslugu i neprestalno je usavrsavati i prilagodjavati potrebama trzista.
            BIT MASTER ce stvoriti ambijent za zaposlene koji ce ih motivisati da daju svoj maksimum i svojom kreativnoscu doprinesu ostvarenju zajednickih ciljeva  (’’You don't build a business, you build people,  and then people build the business’’). 
            <br>
            <br>
            <br>
            <h3>VIZIJA</h3>
            <p>Postati jedan od vodecih centara za obuku i razvoj u oblastima savremenog  <strong>biznisa, menadzmenta,  marketinga i IT-ja.</strong> </p>
            <br>
            <br>
            <br>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <?php include('footer.php'); ?>
        <!-- Jquery and Bootstrap Script files -->
        <script src="lib/jquery-2.0.3.min.js"></script>
        <script src="lib/bootstrap-3.0.3/js/bootstrap.min.js"></script>
    </body>
</html>