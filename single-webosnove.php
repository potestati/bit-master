<!DOCTYPE html>
<html class="demo-1 no-js">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="index.php">
                            <img src="img/logo.png" alt="logo">
                        </a>
                    </div>
                    <?php include 'menu-main.php'; ?>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a> <span class="divider">/</span></li>
                        <li><a href="#">Kategorije</a> <span class="divider">/</span></li>
                        <li class="active">Front End Osnovni Nivo</li>
                    </ul>
                </div>
            </div>
            <div class="inner-img">
                <img src="img/single-web-design.jpg">
            </div>
        </header>

        <div class="partners">
            <div class="container">
                <div class="classroom">
                    Detaljni podaci o kursu
                </div>
                <div id="owl-demo">
                    <div class="item">
                        <p>
                            Trajanje obuke
                            <span>Mesec dana / 2 casa nedeljno</span>                        
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Konsultacije<br>
                            <span>Moguce po dogovoru</span>                      
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Vestine<br>
                            <span>Izrada website</span> 
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Trajanje casa<br>
                            <span>2 skolska casa</span> 
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Cena, vidi cenovnik<br>
                            <span>Plaćanje do 3 rate</span>                         
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Vestine<br>
                            <span>Izrada website</span> 
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Konsultacije<br>
                            <span>Moguce po dogovoru</span>    
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Cena, vidi cenovnik<br>
                            <span>Plaćanje do 3 rate</span>                        
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <a class="rezervisi" href="online-prijava.php">Rezervisi</a>
        <div class="content">
            <h2>Nauci da napravis Website</h2>
            <p>
                Ovo je skola u kojoj ces da naucis da pravis vrhunske Web Portale i Websajt-ove. 
                Udji u svet programiranja pocni sa HTML5 i CSS3. 
                Nauci sta je responsive i napravi responsive sajt koji je funkcionalan na svim aparatima tabletima mobilnim telefonima.
            </p>
            <a class="rezervisi plan" href="http://www.lundyslane.com/">Vise informacija...vidi</a>
            <div class="data">
                <div class="col-sm-4">
                    <img src="img/send.png" alt="send" class="">
                    <h4>Sta Dobijas</h4>
                    <p>Ukoliko zelis da pravis ovakve i slicne moderne Websajtove pocni odmah.
                        Nauci magiju CSS3 , HTML5 , mogucnosti responsive. Nauci da kodiras , udi u svet Informacionih Tehnologija - odmah!</p>
                </div>
                <div class="col-sm-4">
                    <img src="img/geometry.png" alt="send" class="">
                    <h4>Sta Dalje</h4>
                    <p>Za tebe imamo napredne kurseve Front End, gde ces nauciti Bootstrap biblioteku , Uvescemo te u JavaScript, JQUERY , AJAX, nauci sta je json format i kako se sa njim radi na aplikacijama.
                        Ne gubi vreme dok svet odlazi u sve vecu zavisnost od Infromacionih Tehnologija , budi in budi IT - odmah!</p>
                </div>
                <div class="col-sm-4">
                    <img src="img/geography.png" alt="send" class="">
                    <h4>Opšte informacije o kursu</h4>
                    <p>
                        Na kursu se uci izrada websajta kroz praktican rad na izradi samog websajta. Kurs traje mesec dana za to vreme se izradjuju dva websajta kako bi polaznik i utvrdio steceno znanje.
                        Ovaj kurs je namenjen svim ljudima , nije potrebno nikakvo predznanje sem znanja korisnickog rada na racunaru. Tu smo da Vas sve naucimo.
                        Ne gubi vreme iskoristi priliku prijavi se - sada!
                    </p>
                </div>
            </div>
        </div>
        <div class="termini-obuke">
            <span class="fa-stack fa-lg">
                <i class="fa fa-grav fa-5x fa-inverse"></i>
            </span>
            <div class="container">
                <div class="col-md-6">
                    <i class="fa fa-calendar fa-5x"></i>
                    <h4>Klasa I</h4>
                    <p>Početak: 
                        1. februar 2018.

                        Nastava se pohađa sledećim danima: 
                        ponedeljak, utorak, sreda, četvrtak, petak i subota.

                        Vreme: 
                        u periodu od 10h do 22h je radno vreme skole.

                        Nazovite skolu kako biste se dogovorili o tacnim terminima i uslovima. 
                    </p>
                    <a class="rezervisi termini" href="online-prijava.php">Rezervisi</a>
                </div>
                <div class="col-md-6">
                    <i class="fa fa-calendar fa-5x"></i>
                    <h4>Klasa I</h4>
                    <p>Početak: 
                        1. februar 2018.

                        Nastava se pohađa sledećim danima: 
                        ponedeljak, utorak, sreda, četvrtak, petak i subota.

                        Vreme: 
                        u periodu od 10h do 22h je radno vreme skole.

                        Nazovite skolu kako biste se dogovorili o tacnim terminima i uslovima. 
                    </p>
                    <a class="rezervisi termini" href="online-prijava.php">Rezervisi</a>
                </div>
            </div>
        </div>
        <?php include('footer.php'); ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="js/snap.svg-min.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {

                $("#owl-demo").owlCarousel({
                    autoPlay: 3000,
                    mouseDrag: true,
                    items: 4,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [979, 3],
                    navigation: true,
                    navigationText: ["", ""],
                    rewindNav: true,
                    scrollPerPage: false
                });
            });
        </script>
        <!-- / Script-->
    </body>
</html>
