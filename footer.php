<footer>
    <div class="col-sm-4">
        <h3>Skola u kojoj cete nauciti da radite <br> ostalo je na Vama</h3>
        <a href="https://www.facebook.com/Business.IT.master/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/bit_master_ns/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/in/bit-master-372573153/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
        <h4>BIT MASTER</h4>
        Mobilni telefon &#124;
        <a href="tel:+0692434444">
            <span style="color: #cb3438;">069 243 4444</span>
        </a>
        <p>Stražilovska 16 Stražilovska 16, Novi Sad 21000 &#124;<br> Telefon &#58;
            <a href="tel:0213829443">
                <span style="color: #cb3438;">021&#47;3829443</span> &#124;
            </a>
            Email &#58; 
            <a href="mailto:office@bitmaster.rs?Subject=Kursevi%20registracija" target="_top">
                <span style="color: #cb3438;">office&#64;bitmaster&#46;rs</span>
            </a>
        </p>
    </div>
    <div class="col-sm-4">
        <h3>Kursevi</h3>
        <ul>
            <a href="it-courses.php"><li>Programiranje & Front End</li></a>
            <a href="marketing-courses.php"><li>Marketing</li></a>
            <a href="finance-courses.php"><li>Finansije & Racunovodstvo</li></a>
            <a href="it-courses.php"><li>SEO</li></a>
        </ul>
    </div>
    <div class="col-sm-4">
        <h3>Prijavi se sada</h3>
        <a href="register.php" class="btn btn-info" role="button">Online prijava</a>
        <br>
        <a href="register.php" class="btn btn-info" role="button">Registracija</a>
    </div>
</footer>
